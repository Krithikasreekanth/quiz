<%@page import="com.quiz.model.Questions"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
body {
	background-color: #b3f0ff;
}
</style>
</head>
<body style="font-family: Times New Roman">
	<pre>
		<h2 style="font-family: Times New Roman">
			<center>HTML QUIZ</center>
		</h2>
	</pre>
	<div class="container">
		<div class="jumbotron">
			<form:form action="viewform2" method="post" modelAttribute="login">

				<core:forEach var="questionPaper" items="${Listquiz}" begin="0" end="2">
					<br />
					<br />
				
					<div align="left">
						<h4 style="color: #00004d; font-family: Times New Roman">
						<input type="hidden" value="${questionPaper.qid}" name="quesNum"/>
							<b><core:out value="${questionPaper.qid})"></core:out> <core:out
									value="${questionPaper.ques}"></core:out><br /></b>
						</h4>
					</div>
    
         &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick=""
						id="option1" name="option${questionPaper.qid}"
						value="${questionPaper.option1}">
					<core:out value="${questionPaper.option1}"></core:out>
					<br />
     
         &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick=""
						id="option2" name="option${questionPaper.qid}"
						value="${questionPaper.option2}">
					<core:out value="${questionPaper.option2}"></core:out>
					<br />
      
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick=""
						id="option3" name="option${questionPaper.qid}"
						value="${questionPaper.option3}">
					<core:out value="${questionPaper.option3}"></core:out>
					<br />
       
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" onclick=""
						id="option4" name="option${questionPaper.qid}"
						value="${questionPaper.option4}">
					<core:out value="${questionPaper.option4}"></core:out>
					<br />
					

					</pre>

				</core:forEach>
				<input type="submit" name="next" value="next" />

			</form:form>
		</div>
	</div>
</body>
</html>
