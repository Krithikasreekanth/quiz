<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="font-family:Times New Roman; align:center">
<pre><h2 style="font-family:Times New Roman"><center>RESULT PAGE</center></h2></pre>
<div class="container">
 <div class="jumbotran">
<h4>Total no of questions:<core:out value="${Total}"></core:out></h4>
<h4>No of correct questions:<core:out value="${Right}"></core:out></h4>
<h4>No of wrong questions:<core:out value="${Wrong}"></core:out></h4>
<a href="pdf">Result in pdf</a>
</div>
</div>
</body>
</html>