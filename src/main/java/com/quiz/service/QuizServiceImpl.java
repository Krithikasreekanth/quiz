package com.quiz.service;

import java.util.List;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quiz.dao.QuizDao;
import com.quiz.model.Questions;
@Service
@Transactional
public class QuizServiceImpl implements QuizService {
@Autowired
QuizDao quizDao;

public List<Questions> getQuiz()
{
return quizDao.getQuiz();	
}

@Override
public List<Questions> getRightAnswer(int i) {
	// TODO Auto-generated method stub
	return quizDao.getRightAnswer(i);
}

/*public Questions editEmployee(int id)
{
return empDao.editEmployee(id);
	}
public void deleteEmployee(int id)
{
	empDao.deleteEmployee(id);
}
public void updateEmployee(Questions employee)
{
empDao.updateEmployee(employee);

}*/
}
