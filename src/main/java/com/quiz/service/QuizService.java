package com.quiz.service;

import java.util.List;

import com.quiz.model.Questions;

public interface QuizService {
	/*public void addEmployee(Questions ques);
	
	public Questions editEmployee(int id);
	public void deleteEmployee(int id);
	public void updateEmployee(Questions employee)*/
	public List<Questions> getQuiz();

	public List<Questions> getRightAnswer(int i);
	
}
