package com.quiz.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.quiz.model.Questions;
import com.quiz.model.UserLogin;
import com.quiz.service.QuizService;
import com.quiz.validator.QuizValidator;

@Controller
public class QuizController {
	// static int count=0;
	static int r = 0, w = 0;
	@Autowired
	QuizService quizService;
	@Autowired
	QuizValidator quizValidator;

	@RequestMapping(value = "Login")
	public ModelAndView Loginquiz(@ModelAttribute("login") UserLogin login) throws IOException {

		return new ModelAndView("QuizForm");
	}

	@RequestMapping(value = "saveQuiz", method = RequestMethod.POST)
	public ModelAndView Savequiz(@ModelAttribute("login") @Valid UserLogin login, BindingResult result)
			throws IOException {
		quizValidator.validate(login,result);
		if (result.hasErrors()) {
			return new ModelAndView("QuizForm");
		} else {
			List<Questions> Listquiz = quizService.getQuiz();
			return new ModelAndView("ViewForm", "Listquiz", Listquiz);

		}
	}

	@RequestMapping(value = "viewform2")
	public ModelAndView ViewForm(@ModelAttribute("login") UserLogin login, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException {
		List<Questions> Listquiz = quizService.getQuiz();
		int qid = Integer.parseInt(request.getParameter("quesNum"));
		for (int i = qid; i <= 3; i++) {
			String opt = request.getParameter("option" + i);
			List<Questions> list = quizService.getRightAnswer(i);
			String right = list.get(0).getAnswer();
			System.out.println("options:" + opt);
			System.out.println("answers:" + right);
			if (opt.equals(right)) {
				System.out.println("correct");
				r++;
			} else {
				w++;
			}
		}
		session.setAttribute("Total", 5);
		session.setAttribute("Right", r);
		session.setAttribute("Wrong", w);

		return new ModelAndView("ViewForm2", "Listquiz", Listquiz);

	}

	@RequestMapping(value = "SaveResult")
	public ModelAndView SaveResult(@ModelAttribute("login") UserLogin login, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) throws IOException {
		int qid = Integer.parseInt(request.getParameter("quesNum"));
		int fright = Integer.parseInt(request.getParameter("Right"));
		int fwrong = Integer.parseInt(request.getParameter("Wrong"));
		List<Questions> Listquiz = quizService.getQuiz();

		for (int i = qid; i <= 5; i++) {
			String opt = request.getParameter("option" + i);
			List<Questions> list = quizService.getRightAnswer(i);
			String right = list.get(0).getAnswer();
			System.out.println("options:" + opt);
			System.out.println("answers:" + right);
			if (opt.equals(right)) {
				System.out.println("correct");
				fright++;
			}
		}

		System.out.println("wrong");
		session.setAttribute("Total", 5);
		session.setAttribute("Right", fright);
		session.setAttribute("Wrong", (5) - fright);
		return new ModelAndView("ResultForm");
	}
	@RequestMapping(value="pdf")
	public ModelAndView pdfGenerator(HttpServletRequest request,HttpSession session,Model model) throws IOException
	{
		List<Questions> list=quizService.getQuiz();
		return new ModelAndView("UserSummary","list",list);
	}
}
