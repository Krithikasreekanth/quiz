package com.quiz.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.quiz.model.Questions;
@Repository
public class QuizDaoImpl implements QuizDao {
	@Autowired
	SessionFactory sessionFactory;
	/*public void addQuestions(Questions ques)
	{
     sessionFactory.getCurrentSession().save(ques);
	}*/
	public List<Questions> getQuiz()
	{
	return sessionFactory.getCurrentSession().createQuery("from Questions").list();	
	}
	
	public List<Questions> getRightAnswer(int i) {
		
		return sessionFactory.getCurrentSession().createQuery("from Questions where qid=:id").setParameter("id",i).list();
	}
	
/*public Questions editEmployee(int id)
{
	return (Questions) sessionFactory.getCurrentSession().get(Questions.class, id);
}
public void deleteEmployee(int id)
{
	Questions employee=(Questions) sessionFactory.getCurrentSession().get(Questions.class, id);
	if(employee!=null)
	{
		this.sessionFactory.getCurrentSession().delete(employee);
	}
}
public void updateEmployee(Questions employee)
{
	sessionFactory.getCurrentSession().update(employee);
}*/
}
