package com.quiz.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;
@Entity
@Table
public class UserLogin {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
private int id;
//@NotEmpty(message="Name should not be empty")
private String UserName;
//@NotEmpty(message="Password should not be empty")
private String Password;
public UserLogin(int id, String userName, String password) {
	super();
	this.id = id;
	UserName = userName;
	Password = password;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getUserName() {
	return UserName;
}
public void setUserName(String userName) {
	UserName = userName;
}
public String getPassword() {
	return Password;
}
public void setPassword(String password) {
	Password = password;
}
public UserLogin() {
	super();
}

}
