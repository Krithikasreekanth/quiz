package com.quiz.validator;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.quiz.model.UserLogin;

public class QuizValidator implements Validator {
	public boolean supports(Class login) {
        return UserLogin.class.isAssignableFrom(login);
    }

    public void validate(Object target, Errors errors) 
    {
       // ValidationUtils.rejectIfEmptyOrWhitespace(errors, "UserName", "error.UserName", "Username is required.");
        //ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "error.Password", "Password is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "UserName","error.UserName");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "Password","error.Password");

    }


	
}
